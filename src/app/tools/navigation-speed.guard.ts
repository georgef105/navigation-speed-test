import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild } from '@angular/router';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

const NAVIGATION_DELAY = 'navigation-delay';

@Injectable({
  providedIn: 'root'
})
export class NavigationSpeedGuard implements CanActivateChild {
  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    const navigationDelay = +childRoute.queryParams[NAVIGATION_DELAY] || 0;
    return of(true).pipe(
      delay(navigationDelay)
    );
  }
}
