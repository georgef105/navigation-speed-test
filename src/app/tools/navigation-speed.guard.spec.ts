import { TestBed, async, inject } from '@angular/core/testing';

import { NavigationSpeedGuard } from './navigation-speed.guard';

describe('NavigationSpeedGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NavigationSpeedGuard]
    });
  });

  it('should ...', inject([NavigationSpeedGuard], (guard: NavigationSpeedGuard) => {
    expect(guard).toBeTruthy();
  }));
});
