import { Component, OnInit } from '@angular/core';
import { ListingService } from './listing.service';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { DetailedItem } from '../content/content.interface';
import { map, switchMap, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.scss']
})
export class ListingComponent implements OnInit {
  public listing$: Observable<Partial<DetailedItem>>;

  constructor(
    private activatedRoute: ActivatedRoute,
    private listingService: ListingService
  ) { }

  ngOnInit() {
    this.listing$ = this.activatedRoute.params.pipe(
      map(params => +params.listingId),
      switchMap(listingId => this.listingService.getListing(listingId)),
      startWith({})
    );
  }

}
