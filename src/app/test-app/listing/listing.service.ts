import { Injectable } from '@angular/core';
import { ContentService } from '../content/content.service';
import { Observable, timer } from 'rxjs';
import { DetailedItem } from '../content/content.interface';
import { delay, map, switchMap, delayWhen } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

const CONTENT_LOAD_DELAY = 'content-load-delay';

@Injectable({
  providedIn: 'root'
})
export class ListingService {

  constructor(
    private activatedRoute: ActivatedRoute,
    private contentService: ContentService
  ) { }

  public getListing(id: number): Observable<DetailedItem> {
    const contentDelay$ = this.activatedRoute.queryParams.pipe(
      map(queryParams => +queryParams[CONTENT_LOAD_DELAY]),
      switchMap(time => timer(time))
    );

    return this.contentService.getDetailedItem(id).pipe(
      delayWhen(() => contentDelay$)
    );
  }
}
