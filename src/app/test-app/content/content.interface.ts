export interface SearchItem {
  id: number;
  title: string;
  photo: string;
  type: 'cat' | 'dog' | string;
}

export interface DetailedItem extends SearchItem {
  description: string;
  photos: Array<string>;
}

export interface SearchResults {
  list: Array<SearchItem>;
  totalCount: number;
  page: number;
}
