import { DetailedItem } from './content.interface';

export const CONTENT: Array<DetailedItem> = [
  {
    id: 1,
    title: 'Cat No1',
    description: 'Thinking cat',
    photo: '/assets/photo-01.jpg',
    photos: ['/assets/photo-01.jpg'],
    type: 'cat'
  },
  {
    id: 2,
    title: 'Cat No2',
    description: 'Two cats',
    photo: '/assets/photo-02.jpg',
    photos: ['/assets/photo-02.jpg'],
    type: 'cat'
  },
  {
    id: 3,
    title: 'Cat No3',
    description: 'Two cats',
    photo: '/assets/photo-03.jpg',
    photos: ['/assets/photo-03.jpg'],
    type: 'cat'
  },
  {
    id: 4,
    title: 'Cat No4',
    description: 'Cat in hand',
    photo: '/assets/photo-04.jpg',
    photos: ['/assets/photo-04.jpg'],
    type: 'cat'
  }
];
