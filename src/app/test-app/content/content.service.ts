import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

import { DetailedItem, SearchItem } from './content.interface';
import { CONTENT } from './content';

@Injectable({
  providedIn: 'root'
})
export class ContentService {

  public getSearchItems(): Observable<Array<SearchItem>> {
    return of(CONTENT).pipe(
      map(content => content.map(item => ({
        id: item.id,
        title: item.title,
        photo: item.photo,
        type: item.type
      })))
    );
  }

  public getDetailedItem(id: number): Observable<DetailedItem> {
    return of(CONTENT).pipe(
      map(content => content.find(item => item.id === id))
    );
  }
}
