import { Component, OnInit, Input } from '@angular/core';
import { SearchItem } from '../../content/content.interface';

@Component({
  selector: 'app-search-card',
  templateUrl: './search-card.component.html',
  styleUrls: ['./search-card.component.scss']
})
export class SearchCardComponent {
  @Input() public searchItem: SearchItem;
}
