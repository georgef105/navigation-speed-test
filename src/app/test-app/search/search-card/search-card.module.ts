import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchCardComponent } from './search-card.component';

import { MatCardModule } from '@angular/material/card';

@NgModule({
  declarations: [SearchCardComponent],
  imports: [
    CommonModule,
    MatCardModule
  ],
  exports: [SearchCardComponent]
})
export class SearchCardModule { }
