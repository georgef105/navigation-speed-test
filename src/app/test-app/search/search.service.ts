import { Injectable } from '@angular/core';
import { Observable, timer } from 'rxjs';
import { SearchResults } from '../content/content.interface';
import { ContentService } from '../content/content.service';
import { map, delay, delayWhen, switchMap } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

const CONTENT_LOAD_DELAY = 'content-load-delay';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(
    private activatedRoute: ActivatedRoute,
    private contentService: ContentService
  ) { }

  public getSearchResults(): Observable<SearchResults> {
    const contentDelay$ = this.activatedRoute.queryParams.pipe(
      map(queryParams => +queryParams[CONTENT_LOAD_DELAY]),
      switchMap(time => timer(time))
    );

    return this.contentService.getSearchItems().pipe(
      map(searchContent => {
        return {
          list: searchContent,
          totalCount: searchContent.length,
          page: 1
        };
      }),
      delayWhen(() => contentDelay$)
    );
  }
}
