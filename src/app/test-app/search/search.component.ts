import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { SearchResults } from '../content/content.interface';
import { SearchService } from './search.service';
import { startWith } from 'rxjs/operators';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  public searchResults$: Observable<SearchResults>;

  constructor(
    private searchService: SearchService
  ) { }

  ngOnInit() {
    this.searchResults$ = this.searchService.getSearchResults().pipe(
      startWith({
        list: [null, null],
        totalCount: 0,
        page: 1
      })
    );
  }

}
