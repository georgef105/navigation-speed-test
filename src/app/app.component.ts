import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Router, NavigationStart, NavigationEnd, ActivatedRoute } from '@angular/router';
import { map, filter, withLatestFrom } from 'rxjs/operators';

const SHOW_PAGE_LOADING = 'show-page-loading';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public navigating$: Observable<boolean>;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  public ngOnInit(): void {
    const showLoadingIndicator$ = this.activatedRoute.queryParams.pipe(
      map(queryParams => queryParams[SHOW_PAGE_LOADING])
    );

    this.navigating$ = this.router.events.pipe(
      filter(event => event instanceof NavigationStart || event instanceof NavigationEnd),
      withLatestFrom(showLoadingIndicator$),
      map(([event, showLoading]) => {
        return showLoading && event instanceof NavigationStart;
      })
    );
  }
}
