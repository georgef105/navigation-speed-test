import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NavigationSpeedGuard } from './tools/navigation-speed.guard';


const routes: Routes = [
  {
    path: 'config',
    loadChildren: () => import('./config/config.module').then(m => m.ConfigModule)
  },
  {
    path: '',
    children: [
      {
        path: '',
        pathMatch: 'full',
        canActivateChild: [NavigationSpeedGuard],
        loadChildren: () => import('./test-app/home/home.module').then(m => m.HomeModule)
      },
      {
        path: 'search',
        canActivateChild: [NavigationSpeedGuard],
        loadChildren: () => import('./test-app/search/search.module').then(m => m.SearchModule)
      },
      {
        path: 'listing',
        canActivateChild: [NavigationSpeedGuard],
        loadChildren: () => import('./test-app/listing/listing.module').then(m => m.ListingModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { paramsInheritanceStrategy: 'always' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
