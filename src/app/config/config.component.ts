import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';

const NAVIGATION_DELAY = 'navigation-delay';
const CONTENT_LOAD_DELAY = 'content-load-delay';
const SHOW_PAGE_LOADING = 'show-page-loading';

@Component({
  selector: 'app-config',
  templateUrl: './config.component.html',
  styleUrls: ['./config.component.scss']
})
export class ConfigComponent implements OnInit {
  public navigationDelay$: Observable<number>;
  public contentLoadDelay$: Observable<number>;
  public showPageLoading$: Observable<boolean>;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.navigationDelay$ = this.activatedRoute.queryParams.pipe(
      map(queryParams => +queryParams[NAVIGATION_DELAY] || 1)
    );

    this.contentLoadDelay$ = this.activatedRoute.queryParams.pipe(
      map(queryParams => +queryParams[CONTENT_LOAD_DELAY] || 1)
    );

    this.showPageLoading$ = this.activatedRoute.queryParams.pipe(
      map(queryParams => !!queryParams[SHOW_PAGE_LOADING])
    );
  }

  public updateNavigationDelay(value: number): void {
    this.router.navigate([], { queryParams: {
       [NAVIGATION_DELAY]: value
    }, queryParamsHandling: 'merge'});
  }

  public updateContentLoadDelay(value: number): void {
    this.router.navigate([], { queryParams: {
       [CONTENT_LOAD_DELAY]: value
    }, queryParamsHandling: 'merge'});
  }

  public updateShowPageLoading(value: boolean): void {
    this.router.navigate([], { queryParams: {
       [SHOW_PAGE_LOADING]: value || null
    }, queryParamsHandling: 'merge'});
  }
}
